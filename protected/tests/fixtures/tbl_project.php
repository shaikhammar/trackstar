<?php

return array(
    'project1' => array(
        'name' => 'Test Project 1',
        'description' => 'This is test project 1',
        'create_time' => '0000-00-00 00:00:00',
        'create_user_id' => '',
        'update_time' => '0000-00-00 00:00:00',
        'update_user_id' => '',
    ),
    'project2' => array(
        'name' => 'Test Project 2',
        'description' => 'This is test project 2',
        'create_time' => '0000-00-00 00:00:00',
        'create_user_id' => '',
        'update_time' => '0000-00-00 00:00:00',
        'update_user_id' => '',
    ),
    'project3' => array(
        'name' => 'Test Project 3',
        'description' => 'This is test project 3',
        'create_time' => '0000-00-00 00:00:00',
        'create_user_id' => '',
        'update_time' => '0000-00-00 00:00:00',
        'update_user_id' => '',
    ),
);